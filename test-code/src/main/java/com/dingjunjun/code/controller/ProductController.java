package com.dingjunjun.code.controller;

import com.dingjunjun.code.pojo.Product;
import com.dingjunjun.code.service.ProductService;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@RestController
@RequestMapping(value = "/product", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * 查询所有数据
     *
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功！", productService.findAll());
    }

    /**
     * 添加商品
     *
     * @param product
     * @return
     */
    @PostMapping
    public Result saveProduct(@RequestBody Product product) {
        productService.saveProduct(product);
        return new Result(true, StatusCode.OK, "添加成功！");
    }

    /**
     * 根据id查询商品
     *
     * @param id
     * @return
     */
    @GetMapping("/findProductById/{id}")
    public Result findProductById(@PathVariable Integer id) {
        return new Result(true, StatusCode.OK, "查询成功！", productService.findProductById(id));
    }


    /**
     * 批量根据id查询
     * @param ids
     * @return
     */
    @GetMapping("/batchFindProduct/{ids}")
    public Result batchFindProduct(@PathVariable List<Integer> ids) {
        return new Result(true, StatusCode.OK, "批量查询成功！",productService.batchFindProduct(ids));
    }


    /**
     * 根据id删除
     * @param id
     * @return
     */
    @DeleteMapping("/deleteProductById/{id}")
    public Result deleteProductById(@PathVariable Integer id) {
        return new Result(true,StatusCode.OK,"删除成功！",productService.deleteProductById(id));
    }


    /**
     * 批量指定id删除
     * @param ids
     * @return
     */
    @DeleteMapping("/batchDelete/{ids}")
    public Result batchDelete(@PathVariable List<Integer> ids) {
        return new Result(true,StatusCode.OK,"批量删除成功！",productService.batchDelete(ids));
    }


    /**
     * 直接删除单个商品
     * @param product
     * @return
     */
    @GetMapping("/deleteProduct")
    public Result deleteProduct(@RequestBody Product product) {
        return new Result(true,StatusCode.OK,"删除商品成功！",productService.deleteProduct(product));
    }

    /**
     * 修改商品属性
     * @param product
     * @return
     */
    @PutMapping("/updateProduct")
    public Result updateProduct(@RequestBody Product product) {
        return new Result(true, StatusCode.OK, "修改成功！",productService.updateProduct(product));
    }

    /**
     * 批量插入数据
     * @return
     */
    @PostMapping("/batchProduct")
    public Result batchProduct(@RequestBody List<Product> products) {
        return new Result(true, StatusCode.OK, "批量插入数据成功！",productService.batchProduct(products));
    }

    /**
     * 统计商品数据个数
     * @return
     */
    @GetMapping("/countProduct")
    public Result countProduct() {
        return new Result(true, StatusCode.OK, "统计数据成功！",productService.countProduct());
    }
}
