package com.dingjunjun.code.mapper;

import com.dingjunjun.code.pojo.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
public interface ProductMapper extends JpaRepository<Product,Integer>, JpaSpecificationExecutor<Product>{
}
