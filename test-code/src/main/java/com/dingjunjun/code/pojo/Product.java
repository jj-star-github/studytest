package com.dingjunjun.code.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="product")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private double price;
    private Integer stock;
    @Column(name = "INSERTTIME")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd")//解决date格式的时间不能jsonParse的问题
    private Date insertTime;

}
