package com.dingjunjun.code.service;

import com.dingjunjun.code.mapper.ProductMapper;
import com.dingjunjun.code.pojo.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Service
@Slf4j
public class ProductService {

    @Autowired
    private ProductMapper productMapper;

    /**
     * 查询所有表内的信息
     *
     * @return
     */
    public List<Product> findAll() {
        List<Product> productList = productMapper.findAll();
        return productList;
    }


    /**
     * 单个加入商品
     *
     * @param
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveProduct(Product product) {
        product.setId((int) Math.random());
        productMapper.save(product);
    }


    /**
     * 根据id查询商品
     *
     * @param id
     * @return
     */
    public Product findProductById(Integer id) {
        return productMapper.findById(id).get();
    }


    /**
     * 修改商品属性
     *
     * @param product
     */
    @Transactional(rollbackFor = Exception.class)
    public Product updateProduct(Product product) {
        return productMapper.save(product);
    }

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    public boolean deleteProductById(Integer id) {
        boolean flag = true;
        if (flag) {
            productMapper.deleteById(id);
        }
        return flag;
    }


    /**
     * 直接删除单个商品
     *
     * @param product
     * @return
     */
    public boolean deleteProduct(Product product) {
        boolean flag = true;
        if (flag) {
            productMapper.delete(product);
        }
        return flag;
    }

    /**
     * 批量插入数据
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean batchProduct(List<Product>products) {
        if (null != products) {
            productMapper.saveAll(products);
        }
        return true;
    }


    /**
     * 批量根据id查询
     * @param ids
     */
    public List<Product> batchFindProduct(List<Integer> ids) {
        return productMapper.findAllById(ids);
    }


    /**
     * 批量指定id删除
     * @return
     */
    public boolean batchDelete(List<Integer> ids) {
        List<Product> productList = this.batchFindProduct(ids);
        productMapper.deleteAll(productList);
        return true;
    }

    /**
     * 统计商品数据个数
     * @return
     */
    public long countProduct() {
       return productMapper.count();
    }
}
