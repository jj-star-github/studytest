package com.dingjunjun.test;

import com.dingjunjun.code.ProductApplication;
import com.dingjunjun.code.pojo.Product;
import com.dingjunjun.code.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Description:
 * @Version: V1.0
 */
@SpringBootTest(classes = ProductApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProductTest {

    @Autowired
    private ProductService productService;
    @Autowired
    private WebApplicationContext ctx;

    private MockMvc mockMvc;

    /**
     * 初始化 MVC 的环境
     */
    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }


    /**
     * 查询所有商品测试
     */
    @Test
    public void findAllTest() {
        List<Product> productList = productService.findAll();
        System.out.println(productList);
    }


    /**
     * 添加单个商品测试
     */
    @Test
    public void saveProduct() {
        Product product = new Product();
        product.setInsertTime(new Date());
        product.setName("水蜜桃");
        product.setStock(42);
        product.setPrice(19.7);
        productService.saveProduct(product);
    }

    /**
     * 根据id查询单个商品
     */
    @Test
    public void findProductById() {
        System.out.println(productService.findProductById(3));
    }


    /**
     * 根据id删除
     */
    @Test
    public void deleteProductById() {
        System.out.println(productService.deleteProductById(5));
    }

    /**
     * 直接删除单个商品
     */
    @Test
    public void deleteProduct() {
        Product product = new Product();
        product.setId(4);
        //product.setPrice(27.00);
        //product.setStock(32);
        //product.setInsertTime(new Date(2021 - 03 - 11));
        System.out.println(productService.deleteProduct(product));
    }


    /**
     * 修改商品属性
     */
    @Test
    public void updateProduct() {
        Product product = new Product();
        product.setInsertTime(new Date());
        product.setName("水蜜桃");
        product.setStock(22);
        product.setPrice(13.7);
        productService.saveProduct(product);
    }


    /**
     * 批量插入商品数据
     */
    public void batchInsertTest() throws Exception {
       this.mockMvc.perform(post("/batchProduct"))
               .andExpect(status().isCreated());
    }
}
